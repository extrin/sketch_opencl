#include "../CImg.h"
#include <iostream>
#include <stdio.h>
#include <stdlib.h>

using namespace cimg_library;
using namespace std;

void createBlurMask(float * mask, float sigma, int maskSize);

int main()
{
	/* VARIABLES SETUP */

	// ���� � �����������
	const char* imageFileName = "D:\\auto_contrast_processed.bmp";

	// �������� �����������
	CImg<unsigned char> image(imageFileName);

	// ���������� �������� �����������
	int H = image.height();
	int W = image.width();
	const int L = H * W;

	// ��������� �� ������ ��������� �����������
	unsigned char* h_grey_orig_image = image.data();
	//unsigned char* h_grey_inner = new unsigned char[L];
	//unsigned char* h_grey_outer = new unsigned char[L];
	unsigned char* h_grey_res_image = new unsigned char[L];

	int maskSize = 7;
	float sigma_less = 5;
	float sigma_more = sigma_less * 1.6;

	float* h_mask_less = new float[maskSize * maskSize];
	float* h_mask_more = new float[maskSize * maskSize];
	createBlurMask(h_mask_less, sigma_less, maskSize);
	createBlurMask(h_mask_more, sigma_more, maskSize);

	/* SUBTRACTION */
	float* h_mask = new float[maskSize * maskSize];
	for (int i = 0; i < maskSize * maskSize; ++i) {
		h_mask[i] = h_mask_less[i] - h_mask_more[i];
	}

	/* CONVOLUTION */
	for (int x = 0; x < W; ++x)
		for (int y = 0; y < H; ++y) {
			//���������� �����
			float sum = 0.0;

			for (int a = -maskSize / 2; a < maskSize / 2 + 1; ++a) {
				for (int b = -maskSize / 2; b < maskSize / 2 + 1; ++b) {
					float M = h_mask[a + maskSize / 2 + (b + maskSize / 2) * maskSize];
					if (y >= maskSize / 2 && x >= maskSize / 2 && y < (H - maskSize / 2) && x < (W - maskSize / 2)) {
						sum += h_grey_orig_image[(y + b) * W + (x + a)] * M;
					}
				}
			}

			//������� ���������� ������ � �������� �������
			if (y >= maskSize / 2 && x >= maskSize / 2 && y < (H - maskSize / 2) && x < (W - maskSize / 2)) {
				h_grey_res_image[x + y * W] = sum < 0.0 ? 0 : 255;
			}
			else {
				h_grey_res_image[x + y * W] = h_grey_orig_image[x + y * W];
			}
		}

	/* RESULT */

	// ������� �������������� �����������
	CImg<unsigned char> processedImage(W, H, 1, 1, false);
	for (int x = 0; x < W; ++x) {
		for (int y = 0; y < H; ++y) {
			processedImage(x, y) = h_grey_res_image[x + y * W];
		}
	}

	// ��������� ����������� �� ����
	processedImage.save("D:\\difference_of_Gaussians_processed.bmp");

	// ���������� �����������
	CImgDisplay Disp1(image, "Original Image"), Disp2(processedImage, "Processed Image");

	system("pause");
	return 0;
}

void createBlurMask(float * mask, float sigma, int maskSize) {
	float sum = 0.0f;
	for (int a = -maskSize / 2; a < maskSize / 2 + 1; a++) {
		for (int b = -maskSize / 2; b < maskSize / 2 + 1; b++) {
			float temp = exp(-((float)(a * a + b * b) / (2 * sigma * sigma)));
			sum += temp;
			mask[a + maskSize / 2 + (b + maskSize / 2) * maskSize] = temp;
		}
	}
	// ����������
	for (int i = 0; i < maskSize*maskSize; i++)
		mask[i] = mask[i] / sum;

}