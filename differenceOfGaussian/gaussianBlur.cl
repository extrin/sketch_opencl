__kernel void gaussianBlur (
					__global unsigned char* grey_image, 
				    __private int W,
				    __private int H,
				    __global float* mask,
				    __private int maskSize,
				    __global unsigned char* grey_out_image)
{
	int L = H * W;
	int coord = get_global_id(0);
	int x = coord % W;
	int y = (coord - x) / W;

	if (coord < L) {

		//���������� �����
		float sum = 0.0;

		for(int a = -maskSize / 2; a < maskSize / 2 + 1; ++a) {
			for(int b = -maskSize / 2; b < maskSize / 2 + 1; ++b) {
				float M = mask[a + maskSize / 2 + (b + maskSize / 2) * maskSize];
				if (y >= maskSize / 2 && x >= maskSize / 2 && y < (H - maskSize / 2) && x < (W - maskSize / 2)) {
					sum += grey_image[(y + b) * W + (x + a)] * M;
				}
			}
		}

		//������� ���������� ������ � �������� �������
		if (y >= maskSize / 2 && x >= maskSize / 2 && y < (H - maskSize / 2) && x < (W - maskSize / 2)) {
			if (sum <= 0.0) {
				grey_out_image[x + y * W] = 0;
			}
			else {
				grey_out_image[x + y * W] = 255;
			}
		}
		else {
			grey_out_image[x + y * W] = grey_image[x + y * W];
		}

	}
}