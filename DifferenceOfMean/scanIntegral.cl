__kernel void scanStep1(
	__global unsigned char* orig_img,
	__global unsigned int* sum_img,
	__private int width)
{
	uint y = get_global_id(0);
	unsigned int color = 0;
	unsigned int tempVal = 0;
	for (int x = 0; x < width; x++)
	{
		color = orig_img[y * width + x];
		tempVal += color;
		sum_img[y * width + x] = tempVal;
	}
}

__kernel void scanStep2(
	__global unsigned int* sum_img,
	__global unsigned int* res_img,
	__private int height,
	__private int width)
{
	uint x = get_global_id(0);
	unsigned int tempVal = 0;
	unsigned int color = 0;
	for (int y = 0; y < height; y++)
	{
		color = sum_img[x + y * width];
		tempVal += color;
		res_img[x + y * width] = tempVal;
	}
}