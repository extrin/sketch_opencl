__kernel void RGBToGreyScale(
					__global unsigned char* r_image, 
				    __global unsigned char* g_image, 
				    __global unsigned char* b_image,
				    __private int L,
					__global unsigned char* grey_out_image)
{
	int coord = get_global_id(0);
	float multR = 0.299,
		multG = 0.587,
		multB = 0.114;
	grey_out_image[coord] = r_image[coord] * multR + g_image[coord] * multG + b_image[coord] * multB;
}