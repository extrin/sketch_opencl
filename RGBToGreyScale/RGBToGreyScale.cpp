#include <CL/cl.h>
#include "../CImg.h"
#include <iostream>
#include <stdio.h>
#include <stdlib.h>

using namespace cimg_library;																							   
using namespace std;

#define MAX_SOURCE_SIZE (0x100000)

// ��������� ��������������� �������
int waitForEventAndRelease(cl_event *event);
void oclFinish(cl_command_queue *command_queue);
cl_device_id clGetDeviceId();
int loadKernelFile(const char* fileName, size_t* source_size, char* source_str);

int main()
{
	// ���� � �����������
	const char* imageFileName = "D:\\car.bmp";

	// �������� �����������
	CImg<unsigned char> image(imageFileName);
    	
	// ���������� �������� �����������
	int H = image.height();
    int W = image.width();
	const int L = H * W;

	// ���������� ��������, ���������� RGB-���������� �������� �����������
	unsigned char* h_orig_image_r = new unsigned char[L];
	unsigned char* h_orig_image_g = new unsigned char[L];
	unsigned char* h_orig_image_b = new unsigned char[L];
	for ( int y = 0; y < H; y++ )
	{
		for ( int x = 0; x < W; x++ )
		{
			h_orig_image_r[y * W + x] = image(x, y, 0);
			h_orig_image_g[y * W + x] = image(x, y, 1);
			h_orig_image_b[y * W + x] = image(x, y, 2);
		} 
	}

	// ���������� ������� ��� ��������������� �����-������ �����������
	unsigned char* h_grey_image = new unsigned char[L];

	/* ���������� ���������� */
	cl_device_id device_id = NULL;
	cl_context context = NULL;
	cl_command_queue command_queue = NULL;
	cl_mem d_orig_image_r = NULL;
	cl_mem d_orig_image_g = NULL;
	cl_mem d_orig_image_b = NULL;
	cl_mem d_grey_image = NULL;
	cl_program program = NULL;
	cl_kernel kernel = NULL;
	cl_int ret = NULL;
	cl_ulong t_start = 0, 
		t_stop = 0, 
		time = 0, 
		total_time = 0;
	char *source_str = (char*)malloc(MAX_SOURCE_SIZE);
	size_t source_size;

	// ���������� ����������
	device_id = clGetDeviceId();

	// �������� OpenCL ���������
	context = clCreateContext(NULL, 1, &device_id, NULL, NULL, &ret);

	// �������� ���� kernel
	ret = loadKernelFile("./RGBToGreyScale.cl", &source_size, source_str); 

	// �������� ��������� �� ���� ���������
	program = clCreateProgramWithSource(context, 1, (const char **)&source_str, (const size_t *)&source_size, &ret);
	 
	// �������������� ���������
	ret = clBuildProgram(program, 1, &device_id, NULL, NULL, NULL);

	// � ������ ������ ������� ��� �����������
	if (ret == CL_BUILD_PROGRAM_FAILURE) {
		// ���������� ������ ����
		size_t log_size;
		clGetProgramBuildInfo(program, device_id, CL_PROGRAM_BUILD_LOG, 0, NULL, &log_size);

		// �������� ������ ��� ���
		char *log = (char *) malloc(log_size);

		// �������� ���
		clGetProgramBuildInfo(program, device_id, CL_PROGRAM_BUILD_LOG, log_size, log, NULL);

		// ������� ���
		printf("%s\n", log);
		system("pause");
		exit(1);
	}

	// �������� ������� ������
	command_queue = clCreateCommandQueue(context, device_id, CL_QUEUE_PROFILING_ENABLE, &ret);

	// ��������� ������ �� ���������� ��� ����������� � �����
	d_orig_image_r = clCreateBuffer(context, CL_MEM_READ_ONLY, L * sizeof(unsigned char), NULL, &ret);
	d_orig_image_g = clCreateBuffer(context, CL_MEM_READ_ONLY, L * sizeof(unsigned char), NULL, &ret);
	d_orig_image_b = clCreateBuffer(context, CL_MEM_READ_ONLY, L * sizeof(unsigned char), NULL, &ret);
	d_grey_image = clCreateBuffer(context, CL_MEM_WRITE_ONLY, L * sizeof(unsigned char), NULL, &ret);

	// ������� ��� �������� ������
	cl_event wrtEvt_1, wrtEvt_2, wrtEvt_3;

	// ������� ������ �� ����������
	ret = clEnqueueWriteBuffer(command_queue, d_orig_image_r, CL_TRUE, 0, L * sizeof(unsigned char), h_orig_image_r, 0, NULL, &wrtEvt_1);
	ret = clEnqueueWriteBuffer(command_queue, d_orig_image_g, CL_TRUE, 0, L * sizeof(unsigned char), h_orig_image_g, 0, NULL, &wrtEvt_2);
	ret = clEnqueueWriteBuffer(command_queue, d_orig_image_b, CL_TRUE, 0, L * sizeof(unsigned char), h_orig_image_b, 0, NULL, &wrtEvt_3);

	// �������� ��� ������� �� ���������� � ��������� �� ����������
	oclFinish(&command_queue);

	//�������� kernel
	kernel = clCreateKernel(program, "RGBToGreyScale", &ret);	

	//��������� ���������� ������� kernel
	ret = clSetKernelArg(kernel, 0, sizeof(cl_mem), &d_orig_image_r);
	ret = clSetKernelArg(kernel, 1, sizeof(cl_mem), &d_orig_image_g);
	ret = clSetKernelArg(kernel, 2, sizeof(cl_mem), &d_orig_image_b);
	ret = clSetKernelArg(kernel, 3, sizeof(L), &L);
	ret = clSetKernelArg(kernel, 4, sizeof(cl_mem), &d_grey_image);

	// ������ ���������� �������
	size_t global_item_size = L;

	// ���������� kernel	
	cl_event ndrEvt = NULL;
	ret = clEnqueueNDRangeKernel(command_queue, kernel, 1, NULL, &global_item_size, NULL, 0, NULL, &ndrEvt);
	
	// �������� ��� ������� �� ���������� � ��������� �� ����������
	oclFinish(&command_queue);

	// ������� ��� ������
	cl_event readEvt_1;

	//��������� ������ � ����������
	ret = clEnqueueReadBuffer(command_queue, d_grey_image, CL_TRUE, 0, L * sizeof(unsigned char), h_grey_image, 0, NULL, &readEvt_1);

	// �������� ��� ������� �� ���������� � ��������� �� ����������
	oclFinish(&command_queue);

	// ������� �������������� �����������
	CImg<unsigned char> greyImage(W, H, 1, 1, false);
	for (int x = 0; x < W; ++x) {
		for (int y = 0; y < H; ++y) {
			greyImage(x, y) = h_grey_image[x + y * W];
		}
	}

	// ��������� ����������� �� ����
	greyImage.save("D:\\processed.bmp");
	
	// ���������� �����������
	CImgDisplay Disp1(image,"Original Image"),
		Disp2(greyImage,"Processed Image");

	/* ������� */
	ret = clReleaseMemObject(d_orig_image_r);
	ret = clReleaseMemObject(d_orig_image_g);
	ret = clReleaseMemObject(d_orig_image_b);
	ret = clReleaseMemObject(d_grey_image);
	ret = clReleaseProgram(program);
	ret = clReleaseKernel(kernel);
	ret = clReleaseCommandQueue(command_queue);
	ret = clReleaseContext(context); 
	
	// ������������� ������
	free(source_str);
	free(h_orig_image_r);
	free(h_orig_image_g);
	free(h_orig_image_b);
	free(h_grey_image);

	// ����� ����������� ������ �� ���������� � ������������
	ret = clGetEventProfilingInfo(wrtEvt_1, CL_PROFILING_COMMAND_START, sizeof(t_start), &t_start, NULL);
	ret = clGetEventProfilingInfo(wrtEvt_3, CL_PROFILING_COMMAND_END, sizeof(t_stop), &t_stop, NULL);
	time = t_stop - t_start;
	total_time += time;
 	printf("\nCopy memory to device, time in milliseconds = %0.3f ms\n", (time / 1000000.0));
	
	// ����� ���������� ��������� � ������������
	ret = clGetEventProfilingInfo(ndrEvt, CL_PROFILING_COMMAND_START, sizeof(t_start), &t_start, NULL);
	ret = clGetEventProfilingInfo(ndrEvt, CL_PROFILING_COMMAND_END, sizeof(t_stop), &t_stop, NULL);
	time = t_stop - t_start;
	total_time += time;
 	printf("\nExecution time in milliseconds = %0.3f ms\n", (time / 1000000.0));

	// ����� ����������� ������ � ���������� � ������������
	ret = clGetEventProfilingInfo(readEvt_1, CL_PROFILING_COMMAND_START, sizeof(t_start), &t_start, NULL);
	ret = clGetEventProfilingInfo(readEvt_1, CL_PROFILING_COMMAND_END, sizeof(t_stop), &t_stop, NULL);
	time = t_stop - t_start;
	total_time += time;
 	printf("\nCopy memory from device, time in milliseconds = %0.3f ms\n", (time / 1000000.0));

	// ����� ����� ������ � �����������
	printf("\nTotal time = %0.3f ms\n", (total_time / 1000000.0));

	system("pause"); 
	return 0;
}

int waitForEventAndRelease(cl_event *event)
{
    cl_int status = CL_SUCCESS;

	status = clWaitForEvents(1, event);

	switch (status) {
	case CL_INVALID_VALUE:
		printf("waitForEventAndRelease num_events is zero\n");
		system("pause");
		break;
	case CL_INVALID_CONTEXT:
		printf("waitForEventAndRelease events specified in event_list do not belong to the same context\n");
		system("pause");
		break;
	case CL_INVALID_EVENT:
		printf("waitForEventAndRelease event objects specified in event_list are not valid event objects\n");
		system("pause");
		break;
	case CL_EXEC_STATUS_ERROR_FOR_EVENTS_IN_WAIT_LIST:
		printf("waitForEventAndRelease the execution status of any of the events in event_list is a negative integer value\n");
		system("pause");
		break;
	case CL_OUT_OF_RESOURCES:
		printf("waitForEventAndRelease there is a failure to allocate resources required by the OpenCL implementation on the device\n");
		system("pause");
		break;
	case CL_OUT_OF_HOST_MEMORY:
		printf("waitForEventAndRelease there is a failure to allocate resources required by the OpenCL implementation on the host\n");
		system("pause");
		break;
	default:
		printf("waitForEventAndRelease the function was executed successfully, status %i\n", status);
		break;
	}

	status = clReleaseEvent(*event);
	
    return 0;
}

void oclFinish(cl_command_queue *command_queue) {
	int ret = clFinish(*command_queue);
	switch(ret) {
	case CL_INVALID_COMMAND_QUEUE:
		printf("clFinish command_queue is not a valid command-queue\n");
		break;
	case CL_OUT_OF_HOST_MEMORY:
		printf("clFinish there is a failure to allocate resources required by the OpenCL implementation on the host\n");
		break;
	case CL_OUT_OF_RESOURCES:
		printf("clFinish there is a failure to allocate resources required by the OpenCL implementation on the device\n");
		break;
	default:
		printf("clFinish the function call was executed successfully code %i\n", ret);
		break;
	}
}

cl_device_id clGetDeviceId() {
	char device_name[80];
	char vendor_name[80];

	cl_device_id device_id = NULL;
	cl_platform_id platform_id = NULL;
	cl_uint ret_num_devices, get_num_devices;
	cl_uint ret_num_platforms, get_num_platforms;
	cl_int ret = NULL;

	// ��������� ���������� ��������� �������� � ���������
	ret = clGetPlatformIDs(1, &platform_id, &get_num_platforms);

	// �������� ������ ��������� ��������
	cl_platform_id* platform_ids = (cl_platform_id*)malloc(sizeof(cl_platform_id) * get_num_platforms);
	ret = clGetPlatformIDs(get_num_platforms, platform_ids, &ret_num_platforms);

	// �������� ����� �������� ��������
	for (int i = 0; i < get_num_platforms; ++i) {
		ret = clGetPlatformInfo(platform_ids[i], CL_PLATFORM_VENDOR, 80, vendor_name, NULL);
 		printf("%d - %s\n", i, vendor_name);
	}

	int p_id = 0;
	// ���� ���������� ������ ����� ���������, ������ ������� ����
	if (get_num_platforms > 1) {
		
		do {
			printf("Please input platform ID; default is 0.\nPlatform ID: ");
			scanf("%d", &p_id);
		} while (p_id < 0 || p_id > (ret_num_platforms - 1));

	}
	
	// ������� ���������� ��������� ��������� ���������
	ret = clGetDeviceIDs(platform_ids[p_id], CL_DEVICE_TYPE_ALL, 1, &device_id, &get_num_devices);

	// �������� ������ ��� ������ ���������� ���������
	cl_device_id* device_ids = (cl_device_id*)malloc(sizeof(cl_device_id) * get_num_devices);

	// �������� ������ ���� ��������� OpenCL ��������� ��� ���������
	ret = clGetDeviceIDs(platform_ids[p_id], CL_DEVICE_TYPE_ALL, get_num_devices, device_ids, &ret_num_devices);

	// ������� ������ ���������
	for (int i = 0; i < get_num_devices; ++i) {
		ret = clGetDeviceInfo(device_ids[i], CL_DEVICE_NAME, 80, device_name, NULL);
 		printf("%d - %s\n", i, device_name);
	}

	int dev_id = 0;
	// ���������� ������� ����������, ���� �� ������ 1
	if (get_num_devices > 1) {

		do {
			printf("Please input device ID; default is 0.\nDevice ID: ");
			scanf("%d", &dev_id);
			printf("\n");
		} while (dev_id < 0 || dev_id > (ret_num_devices - 1));

	}

	// ������� ���������� � ��������� ����������
	ret = clGetDeviceInfo(device_ids[dev_id], CL_DEVICE_NAME, 80, device_name, NULL);
	cout << "Device Name " << device_name << endl;

	cl_uint maxComputeUnits;
	ret = clGetDeviceInfo(device_ids[dev_id], CL_DEVICE_MAX_COMPUTE_UNITS, sizeof(cl_uint), &maxComputeUnits, NULL);
	cout << "Compute Units " << maxComputeUnits << endl;

	cl_uint maxClockFrequency;
	ret = clGetDeviceInfo(device_ids[dev_id], CL_DEVICE_MAX_CLOCK_FREQUENCY, sizeof(cl_uint), &maxClockFrequency, NULL);
	cout << "Boost Clock " << maxClockFrequency << "MHz" << endl;

	return device_ids[dev_id];
}

int loadKernelFile(const char* fileName, size_t* source_size, char* source_str) {
	// ���������� � �������� kernel
	FILE *fp;
	 
	// �������� ��������� ����, ������������ � kernel
	fp = fopen(fileName, "r");
	if (!fp) 
	{
		fprintf(stderr, "Failed to load kernel.\n");
		return 1;
	}

	*source_size = fread(source_str, 1, MAX_SOURCE_SIZE, fp);
	fclose(fp);
	return 0;
}