__kernel void histogram(
					__global unsigned char* grey_image, 
					__global unsigned long* histogram)
{
	int coord = get_global_id(0);
	histogram[grey_image[coord]]++;
}