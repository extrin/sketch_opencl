__kernel void autoContrast(
					__global unsigned char* grey_orig_image, 
					__global unsigned char* LUT,
					__global unsigned char* grey_res_image)
{
	int coord = get_global_id(0);
	grey_res_image[coord] = LUT[grey_orig_image[coord]];
}