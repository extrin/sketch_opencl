
class Stack   //��� ��������� FloodFill
{
    int X,Y;
    Stack *Next;
public:
	int GetX () const;
    int GetY () const;
	friend Stack* Push(Stack*,int,int);
	friend Stack* Pop(Stack*);
};

//-----------------------------------------------------------------------------------------
//                             ������� ��������
//-----------------------------------------------------------------------------------------
int FloodFill(unsigned char* Matrix,bool* Ind,int XStart,int YStart,const int H,const int W,bool fill)//���������� �������
{
	int S=0;//��� ����������� �������
	Stack *Head=Push(nullptr,XStart,YStart); //������� ���� 
	while (Head)
	{
		int X=Head->GetX();
		int y=Head->GetY();
		Ind[y*W+X]=true;//��������, ��� �� ����� ����
		S++;//����������� �������
		if (fill)//���� ����� ������ �����
			Matrix[y*W+X]=0;
		Head=Pop(Head);

		int xl=X-1;
		int xr=X+1;

		if (Ind[y*W+xl])
			continue;   

		//��������� � ����� �����
		while( xl >= 0 && (Matrix[y*W+xl]!=0 && !(Ind[y*W+xl])))//�� ������� � �� ���� ����������������
		{
			Ind[y*W+xl] = true;
			S++;
			if (fill)//���� ����� ������ �����
				Matrix[y*W+xl]=0;
			xl--;
		}
            ++xl;   //������������ � ���������� �������

            //��������� � ������ �����
            while( xr < W && Matrix[y*W+xr]!=0 && !(Ind[y*W+xr]) )
			{
				Ind[y*W+xr] = true;
				S++;
				if (fill)//���� ����� ������ �����
					Matrix[y*W+xr]=0;
				xr++;
			}
            --xr;

			//��������� ������� ����� (���� ��� ����������)
            bool bLeftBack = true;
            if( y > 0 )
                for( int x = xl; x <= xr; ++x )
                {
                    if(Matrix[(y-1)*W+x]!=0 && !(Ind[(y-1)*W+x]))
                    {
                        if( bLeftBack )
                            Head=Push(Head,x,y-1);
                        bLeftBack = false; 
                    }
                    else
                        bLeftBack = true;
                }
				 //��������� ������ ����� (���� ��� ����������)
            bLeftBack = true;
            if (y < H )
                for( int x = xl; x <= xr; ++x )
                {
                    // check for being out of zone
                    if (Matrix[(y+1)*W+x]!=0 && !(Ind[(y+1)*W+x]))
                    {
                        if( bLeftBack )
                            Head=Push(Head,x,y+1);
                        bLeftBack = false;
                    }
                    else
                        bLeftBack = true;
                }
	}
	return S;
}
//-----------------------------------------------------------------------------------------
//                             ���������� �������� � ����
//-----------------------------------------------------------------------------------------
Stack *Push(Stack *head,int Xnew,int Ynew)    
{
	Stack *NewElement=new Stack;
	NewElement->Next=head;
	NewElement->X=Xnew;
	NewElement->Y=Ynew;
	return NewElement;
}
//-----------------------------------------------------------------------------------------
//                             ������� �������� �� �����
//-----------------------------------------------------------------------------------------
Stack *Pop(Stack *head) 
{
	if (head)
	{
	Stack *pElement=head->Next;
    delete head;
	return pElement;
	}
	return nullptr;
}
//-----------------------------------------------------------------------------------------
//                             Stack::GetX
//-----------------------------------------------------------------------------------------
int Stack::GetX () const
{
	return X;
}
//-----------------------------------------------------------------------------------------
//                             Stack::GetY
//-----------------------------------------------------------------------------------------
int Stack::GetY () const
{
	return Y;
}
