__kernel void RGBToGreyScale(
	__global uchar* r_image,
	__global uchar* g_image,
	__global uchar* b_image,
	__private int L,
	__global uchar* grey_out_image)
{
	int coord = get_global_id(0);
	float multR = 0.299,
		multG = 0.587,
		multB = 0.114;
	grey_out_image[coord] = r_image[coord] * multR + g_image[coord] * multG + b_image[coord] * multB;
}

__kernel void histogram(
	__global uchar* grey_image,
	__global ulong* histogram)
{
	int coord = get_global_id(0);
	histogram[grey_image[coord]]++;
}

__kernel void autoContrast(
	__global uchar* grey_orig_image,
	__global uchar* LUT,
	__global uchar* grey_res_image)
{
	int coord = get_global_id(0);
	grey_res_image[coord] = LUT[grey_orig_image[coord]];
}

__kernel void scanStep1(
	__global uchar* orig_img,
	__global uint* sum_img,
	__private int width)
{
	uint y = get_global_id(0);
	uint color = 0;
	uint tempVal = 0;
	for (int x = 0; x < width; x++)
	{
		color = orig_img[y * width + x];
		tempVal += color;
		sum_img[y * width + x] = tempVal;
	}
}

__kernel void scanStep2(
	__global uint* sum_img,
	__global uint* res_img,
	__private int height,
	__private int width)
{
	uint x = get_global_id(0);
	uint tempVal = 0;
	uint color = 0;
	for (int y = 0; y < height; y++)
	{
		color = sum_img[x + y * width];
		tempVal += color;
		res_img[x + y * width] = tempVal;
	}
}

__kernel void diffOfMeans(
	__global uint* integral_img,
	__global uchar* res_img,
	__private int width,
	__private float num_mean_1,
	__private float num_mean_2,
	__private float step)
{
	int coord = get_global_id(0);
	int x = coord % width;
	int y = coord / width;
	int hs_1 = 1;
	int hs_2 = 2;

	float num1 = num_mean_1 * num_mean_1;
	float num2 = num_mean_2 * num_mean_2;

	uint sum = 0;

	if ((x - hs_1) > 0 && (y - hs_1) > 0) {
		sum = integral_img[(x + hs_1) + (y + hs_1) * width]
			+ integral_img[(x - hs_1 - 1) + (y - hs_1 - 1) * width]
			- integral_img[(x - hs_1 - 1) + (y + hs_1) * width]
			- integral_img[(x + hs_1) + (y - hs_1 - 1) * width];
	}
	else sum = integral_img[(x + hs_1) + (y + hs_1) * width];

	float first_mean = sum / num1;
	sum = 0;

	if ((x - hs_2) > 0 && (y - hs_2) > 0) {
		sum = integral_img[(x + hs_2) + (y + hs_2) * width]
			+ integral_img[(x - hs_2 - 1) + (y - hs_2 - 1) * width]
			- integral_img[(x - hs_2 - 1) + (y + hs_2) * width]
			- integral_img[(x + hs_2) + (y - hs_2 - 1) * width];
	}
	else sum = integral_img[(x + hs_2) + (y + hs_2) * width];

	float second_mean = sum / num2;

	float p = first_mean - second_mean;

	res_img[x + y * width] = p > step ? 255 : 0;
}

__kernel void dilation(
	__global uchar* grey_image,
	__private int W,
	__private int H,
	__global uchar* temp_image)
{
	int coord = get_global_id(0);
	int x = coord % W;
	int y = (coord - x) / W;

	if (x > 0 && x < (W - 1) && y > 0 && y < (H - 1)) {

		if (grey_image[coord] == 255) {
			for (int l = -1; l < 2; ++l) {
				for (int t = -1; t < 2; ++t) {
					temp_image[(y + l) * W + (x + t)] = 255;
				}
			}
		}

	}
}

__kernel void erosia(
	__global uchar* temp_image,
	__private int W,
	__private int H,
	__global uchar* grey_out_image)
{
	int coord = get_global_id(0);
	int x = coord % W;
	int y = (coord - x) / W;

	if (x > 0 && x < (W - 1) && y > 0 && y < (H - 1)) {

		if (temp_image[coord] == 0) {
			grey_out_image[coord] = 0;
		}
		else {
			grey_out_image[coord] = 255;
			for (int l = -1; l < 2; ++l) {
				for (int t = -1; t < 2; ++t) {
					if (temp_image[(y + l) * W + (x + t)] == 0) {
						grey_out_image[coord] = 0;
						break;
					}
				}
			}
		}

	}
}

__kernel void blending(
	__global uchar* mask,
	__global uchar* orig_r,
	__global uchar* orig_g,
	__global uchar* orig_b,
	__global uchar* res_r,
	__global uchar* res_g,
	__global uchar* res_b)
{
	uint coord = get_global_id(0);

	if (mask[coord] > 0) {
		res_r[coord] = orig_r[coord];
		res_g[coord] = orig_g[coord];
		res_b[coord] = orig_b[coord];
	}
	else {
		res_r[coord] = 0;
		res_g[coord] = 0;
		res_b[coord] = 0;
	}
}