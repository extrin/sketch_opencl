#include <CL/cl.h>
#include "../CImg.h"
#include <iostream>
#include <stdio.h>
#include <stdlib.h>

using namespace cimg_library;
using namespace std;

#define MAX_SOURCE_SIZE (0x100000)

void oclFinish(cl_command_queue *command_queue);
cl_device_id clGetDeviceId();
int loadKernelFile(const char* fileName, size_t* source_size, char* source_str);

int main()
{
	/* VARIABLES SETUP */

	// ���� � �����������
	const char* imageFileName = "D:\\close_op_processed.bmp";

	// �������� �����������
	CImg<cl_uchar> image(imageFileName);

	// ���������� �������� �����������
	cl_uint H = image.height();
	cl_uint W = image.width();
	cl_uint bW = W / 2;
	cl_uint bH = H / 2;
	const cl_uint L = H * W;
	const cl_uint bL = bH * bW;

	// ��������� �� ������ ��������� �����������
	cl_uchar* h_grey_orig_image = image.data();

	// ���������� ������� ��� ��������������� �����-������ �����������
	cl_uchar* h_grey_res_image = new cl_uchar[L];

	/* ���������� ���������� */
	cl_device_id device_id = NULL;
	cl_context context = NULL;
	cl_command_queue command_queue = NULL;
	cl_mem d_grey_orig_image = NULL;
	cl_mem d_grey_res_image = NULL;
	cl_mem d_bLabels = NULL;
	cl_mem d_labels = NULL;
	cl_mem d_bConn = NULL;
	cl_mem d_isChanged = NULL;
	cl_mem d_block_hist = NULL;
	cl_program program = NULL;
	cl_kernel BlockMapInitKernel = NULL,
		ScanningKernel = NULL,
		AnalysisKernel = NULL,
		BlockHistKernel = NULL,
		FinalLabelsKernel = NULL,
		SuppressionKernel = NULL;
	size_t global_item_size;
	cl_int ret = NULL;
	cl_ulong t_start = 0,
		t_stop = 0,
		time_ex = 0,
		time_write = 0,
		time_read = 0,
		total_time = 0;
	char *source_str = (char*)malloc(MAX_SOURCE_SIZE);
	size_t source_size;

	/* DEVICE SETUP */

	// ���������� ����������
	device_id = clGetDeviceId();

	// �������� OpenCL ���������
	context = clCreateContext(NULL, 1, &device_id, NULL, NULL, &ret);

	// �������� ������� ������
	command_queue = clCreateCommandQueue(context, device_id, CL_QUEUE_PROFILING_ENABLE, &ret);

	// ��������� ������ �� ���������� ��� ����������� � �����
	d_grey_orig_image = clCreateBuffer(context, CL_MEM_READ_ONLY, L * sizeof(cl_uchar), NULL, &ret);
	d_grey_res_image = clCreateBuffer(context, CL_MEM_WRITE_ONLY, L * sizeof(cl_uchar), NULL, &ret);
	d_bLabels = clCreateBuffer(context, CL_MEM_READ_WRITE, bL * sizeof(cl_uint), NULL, &ret);
	d_bConn = clCreateBuffer(context, CL_MEM_READ_WRITE, bL * sizeof(cl_uchar), NULL, &ret);
	d_isChanged = clCreateBuffer(context, CL_MEM_WRITE_ONLY, sizeof(cl_uint), NULL, &ret);
	d_block_hist = clCreateBuffer(context, CL_MEM_READ_WRITE, (bL + 1) * sizeof(cl_uint), NULL, &ret);
	d_labels = clCreateBuffer(context, CL_MEM_READ_WRITE, L * sizeof(cl_uint), NULL, &ret);

	cl_event wrtEvt_1;
	// ������� ������ �� ����������
	ret = clEnqueueWriteBuffer(command_queue, d_grey_orig_image, CL_TRUE, 0, L * sizeof(cl_uchar), h_grey_orig_image, 0, NULL, &wrtEvt_1);

	// �������� ��� ������� �� ���������� � ��������� �� ����������
	oclFinish(&command_queue);

	/* LABELLING STEP 1 */
	cl_event ndrEvt_1;
	// �������� ���� kernel
	ret = loadKernelFile("./suppression.cl", &source_size, source_str);

	// �������� ��������� �� ���� ���������
	program = clCreateProgramWithSource(context, 1, (const char **)&source_str, (const size_t *)&source_size, &ret);

	// �������������� ���������
	ret = clBuildProgram(program, 1, &device_id, NULL, NULL, NULL);

	// � ������ ������ ������� ��� �����������
	if (ret != CL_SUCCESS) {
		// ���������� ������ ����
		size_t log_size;
		clGetProgramBuildInfo(program, device_id, CL_PROGRAM_BUILD_LOG, 0, NULL, &log_size);

		// �������� ������ ��� ���
		char *log = (char *)malloc(log_size);

		// �������� ���
		clGetProgramBuildInfo(program, device_id, CL_PROGRAM_BUILD_LOG, log_size, log, NULL);

		// ������� ���
		printf("%s\n", log);
		system("pause");
		exit(1);
	}

	//�������� kernel
	BlockMapInitKernel = clCreateKernel(program, "blockMapInit", &ret);

	//��������� ���������� ������� kernel
	ret = clSetKernelArg(BlockMapInitKernel, 0, sizeof(cl_mem), &d_grey_orig_image);
	ret = clSetKernelArg(BlockMapInitKernel, 1, sizeof(cl_mem), &d_bLabels);
	ret = clSetKernelArg(BlockMapInitKernel, 2, sizeof(cl_mem), &d_bConn);
	ret = clSetKernelArg(BlockMapInitKernel, 3, sizeof(cl_uint), &bW);

	ScanningKernel = clCreateKernel(program, "scanningPhase", &ret);

	//��������� ���������� ������� kernel
	ret = clSetKernelArg(ScanningKernel, 0, sizeof(cl_mem), &d_bLabels);
	ret = clSetKernelArg(ScanningKernel, 1, sizeof(cl_mem), &d_bConn);
	ret = clSetKernelArg(ScanningKernel, 2, sizeof(cl_uint), &bW);
	ret = clSetKernelArg(ScanningKernel, 3, sizeof(cl_mem), &d_isChanged);

	AnalysisKernel = clCreateKernel(program, "analysisPhase", &ret);

	//��������� ���������� ������� kernel
	ret = clSetKernelArg(AnalysisKernel, 0, sizeof(cl_mem), &d_bLabels);
	ret = clSetKernelArg(AnalysisKernel, 1, sizeof(cl_mem), &d_isChanged);

	BlockHistKernel = clCreateKernel(program, "label_histogram", &ret);

	//��������� ���������� ������� kernel
	ret = clSetKernelArg(BlockHistKernel, 0, sizeof(cl_mem), &d_labels);
	ret = clSetKernelArg(BlockHistKernel, 1, sizeof(cl_mem), &d_block_hist);

	FinalLabelsKernel = clCreateKernel(program, "SetFinalLabels", &ret);
	ret = clSetKernelArg(FinalLabelsKernel, 0, sizeof(cl_mem), &d_grey_orig_image);
	ret = clSetKernelArg(FinalLabelsKernel, 1, sizeof(cl_mem), &d_bLabels);
	ret = clSetKernelArg(FinalLabelsKernel, 2, sizeof(cl_mem), &d_labels);
	ret = clSetKernelArg(FinalLabelsKernel, 3, sizeof(cl_uint), &W);

	SuppressionKernel = clCreateKernel(program, "suppression", &ret);

	//��������� ���������� ������� kernel
	ret = clSetKernelArg(SuppressionKernel, 0, sizeof(cl_mem), &d_grey_res_image);
	ret = clSetKernelArg(SuppressionKernel, 1, sizeof(cl_mem), &d_labels);
	ret = clSetKernelArg(SuppressionKernel, 2, sizeof(cl_mem), &d_block_hist);
	ret = clSetKernelArg(SuppressionKernel, 3, sizeof(cl_uint), &W);

	// ������ ���������� �������
	global_item_size = bL;

	// ���������� kernel	
	ret = clEnqueueNDRangeKernel(command_queue, BlockMapInitKernel, 1, NULL, &global_item_size, NULL, 0, NULL, &ndrEvt_1);

	// �������� ��� ������� �� ���������� � ��������� �� ����������
	oclFinish(&command_queue);

	cl_event ndrEvt_2, ndrEvt_3;
	cl_uint h_isChanged;
	
	cl_event wrtEvt_2;
	cl_event readEvt_1;
	cl_uint iter = 0;

	do {

		if (iter > 10) break;
		/* LABELLING STEP 2 */

		h_isChanged = 0;
		ret = clEnqueueWriteBuffer(command_queue, d_isChanged, CL_TRUE, 0, sizeof(cl_uint), &h_isChanged, 0, NULL, &wrtEvt_2);

		// �������� ��� ������� �� ���������� � ��������� �� ����������
		oclFinish(&command_queue);

		// ���������� kernel	
		ret = clEnqueueNDRangeKernel(command_queue, ScanningKernel, 1, NULL, &global_item_size, NULL, 0, NULL, &ndrEvt_2);

		// �������� ��� ������� �� ���������� � ��������� �� ����������
		oclFinish(&command_queue);

		/* LABELLING STEP 3 */

		// ���������� kernel	
		ret = clEnqueueNDRangeKernel(command_queue, AnalysisKernel, 1, NULL, &global_item_size, NULL, 0, NULL, &ndrEvt_3);

		// �������� ��� ������� �� ���������� � ��������� �� ����������
		oclFinish(&command_queue);

		ret = clEnqueueReadBuffer(command_queue, d_isChanged, CL_TRUE, 0, sizeof(cl_uint), &h_isChanged, 0, NULL, &readEvt_1);
		// �������� ��� ������� �� ���������� � ��������� �� ����������
		oclFinish(&command_queue);

		++iter;
	} while (h_isChanged);

	/* SET FINAL LABELS */

	cl_uint* h_labels = new cl_uint[L];
	memset(h_labels, 0, L * sizeof(cl_uint));
	cl_event wrtEvt_3;
	// ������� ������ �� ����������
	ret = clEnqueueWriteBuffer(command_queue, d_labels, CL_TRUE, 0, L * sizeof(cl_uint), h_labels, 0, NULL, &wrtEvt_3);
	// �������� ��� ������� �� ���������� � ��������� �� ����������
	oclFinish(&command_queue);

	cl_event ndrEvt_4;
	global_item_size = L;
	// ���������� kernel	
	ret = clEnqueueNDRangeKernel(command_queue, FinalLabelsKernel, 1, NULL, &global_item_size, NULL, 0, NULL, &ndrEvt_4);

	// �������� ��� ������� �� ���������� � ��������� �� ����������
	oclFinish(&command_queue);


	/* HISTOGRAM OF LABELS */
	cl_uint* h_block_hist = new cl_uint[bL + 1];
	memset(h_block_hist, 0, sizeof(cl_uint)* (bL + 1));
	cl_event wrtEvt_4;
	// ������� ������ �� ����������
	ret = clEnqueueWriteBuffer(command_queue, d_block_hist, CL_TRUE, 0, (bL + 1) * sizeof(cl_uint), h_block_hist, 0, NULL, &wrtEvt_4);
	// �������� ��� ������� �� ���������� � ��������� �� ����������
	oclFinish(&command_queue);

	cl_event ndrEvt_5;
	// ���������� kernel	
	ret = clEnqueueNDRangeKernel(command_queue, BlockHistKernel, 1, NULL, &global_item_size, NULL, 0, NULL, &ndrEvt_5);

	// �������� ��� ������� �� ���������� � ��������� �� ����������
	oclFinish(&command_queue);

	/* SUPPRESSION */

	cl_event ndrEvt_6;
	// ���������� kernel	
	ret = clEnqueueNDRangeKernel(command_queue, SuppressionKernel, 1, NULL, &global_item_size, NULL, 0, NULL, &ndrEvt_6);

	// �������� ��� ������� �� ���������� � ��������� �� ����������
	oclFinish(&command_queue);

	/* RESULT */

	// ������� ��� ������
	cl_event readEvt_2;

	//��������� ������ � ����������
	ret = clEnqueueReadBuffer(command_queue, d_grey_res_image, CL_TRUE, 0, L * sizeof(cl_uchar), h_grey_res_image, 0, NULL, &readEvt_2);

	// �������� ��� ������� �� ���������� � ��������� �� ����������
	oclFinish(&command_queue);

	// ������� �������������� �����������
	CImg<unsigned char> processedImage(W, H, 1, 1, false);
	for (int x = 0; x < W; ++x) {
		for (int y = 0; y < H; ++y) {
			processedImage(x, y) = h_grey_res_image[x + y * W];
		}
	}

	// ��������� ����������� �� ����
	processedImage.save("D:\\complex_suppression_processed.bmp");

	// ���������� �����������
	CImgDisplay Disp1(image, "Original Image"), Disp2(processedImage, "Processed Image");

	/* TIME MEASUREMENT */

	// ����� ����������� ������ �� ���������� � ������������
	ret = clGetEventProfilingInfo(wrtEvt_1, CL_PROFILING_COMMAND_START, sizeof(t_start), &t_start, NULL);
	ret = clGetEventProfilingInfo(wrtEvt_1, CL_PROFILING_COMMAND_END, sizeof(t_stop), &t_stop, NULL);
	time_write += t_stop - t_start;
	total_time += time_write;
	printf("\nCopy memory to device, time in milliseconds = %0.3f ms\n", (time_write / 1000000.0));

	// ����� ���������� ��������� � ������������
	ret = clGetEventProfilingInfo(ndrEvt_1, CL_PROFILING_COMMAND_START, sizeof(t_start), &t_start, NULL);
	ret = clGetEventProfilingInfo(ndrEvt_1, CL_PROFILING_COMMAND_END, sizeof(t_stop), &t_stop, NULL);
	time_ex += t_stop - t_start;

	ret = clGetEventProfilingInfo(ndrEvt_2, CL_PROFILING_COMMAND_START, sizeof(t_start), &t_start, NULL);
	ret = clGetEventProfilingInfo(ndrEvt_2, CL_PROFILING_COMMAND_END, sizeof(t_stop), &t_stop, NULL);
	time_ex += t_stop - t_start;

	ret = clGetEventProfilingInfo(ndrEvt_3, CL_PROFILING_COMMAND_START, sizeof(t_start), &t_start, NULL);
	ret = clGetEventProfilingInfo(ndrEvt_3, CL_PROFILING_COMMAND_END, sizeof(t_stop), &t_stop, NULL);
	time_ex += t_stop - t_start;

	ret = clGetEventProfilingInfo(ndrEvt_4, CL_PROFILING_COMMAND_START, sizeof(t_start), &t_start, NULL);
	ret = clGetEventProfilingInfo(ndrEvt_4, CL_PROFILING_COMMAND_END, sizeof(t_stop), &t_stop, NULL);
	time_ex += t_stop - t_start;

	ret = clGetEventProfilingInfo(ndrEvt_5, CL_PROFILING_COMMAND_START, sizeof(t_start), &t_start, NULL);
	ret = clGetEventProfilingInfo(ndrEvt_5, CL_PROFILING_COMMAND_END, sizeof(t_stop), &t_stop, NULL);
	time_ex += t_stop - t_start;

	total_time += time_ex;
	printf("\nExecution time in milliseconds = %0.3f ms\n", (time_ex / 1000000.0));

	// ����� ����� ������ � �����������
	printf("\nTotal time on device = %0.3f ms\n", (total_time / 1000000.0));

	// ����� ����� ����������� ������ ���������, ������� � ������ ������ ������ �� ���������� ������ ������
	ret = clGetEventProfilingInfo(wrtEvt_1, CL_PROFILING_COMMAND_START, sizeof(t_start), &t_start, NULL);
	printf("\nTotal time of program working = %0.3f ms\n", ((t_stop - t_start) / 1000000.0));

	printf("\nTotal iterations = %i\n", iter);

	system("pause");
	return 0;
}

void oclFinish(cl_command_queue *command_queue) {
	int ret = clFinish(*command_queue);
	switch (ret) {
	case CL_INVALID_COMMAND_QUEUE:
		printf("clFinish command_queue is not a valid command-queue\n");
		break;
	case CL_OUT_OF_HOST_MEMORY:
		printf("clFinish there is a failure to allocate resources required by the OpenCL implementation on the host\n");
		break;
	case CL_OUT_OF_RESOURCES:
		printf("clFinish there is a failure to allocate resources required by the OpenCL implementation on the device\n");
		break;
	default:
		printf("clFinish the function call was executed successfully code %i\n", ret);
		break;
	}
}

cl_device_id clGetDeviceId() {
	char device_name[80];
	char vendor_name[80];

	cl_device_id device_id = NULL;
	cl_platform_id platform_id = NULL;
	cl_uint ret_num_devices, get_num_devices;
	cl_uint ret_num_platforms, get_num_platforms;
	cl_int ret = NULL;

	// ��������� ���������� ��������� �������� � ���������
	ret = clGetPlatformIDs(1, &platform_id, &get_num_platforms);

	// �������� ������ ��������� ��������
	cl_platform_id* platform_ids = (cl_platform_id*)malloc(sizeof(cl_platform_id)* get_num_platforms);
	ret = clGetPlatformIDs(get_num_platforms, platform_ids, &ret_num_platforms);

	// �������� ����� �������� ��������
	for (cl_uint i = 0; i < get_num_platforms; ++i) {
		ret = clGetPlatformInfo(platform_ids[i], CL_PLATFORM_VENDOR, 80, vendor_name, NULL);
		printf("%d - %s\n", i, vendor_name);
	}

	int p_id = 0;
	// ���� ���������� ������ ����� ���������, ������ ������� ����
	if (get_num_platforms > 1) {

		do {
			printf("Please input platform ID; default is 0.\nPlatform ID: ");
			scanf("%d", &p_id);
		} while (p_id < 0 || p_id >(ret_num_platforms - 1));

	}

	// ������� ���������� ��������� ��������� ���������
	ret = clGetDeviceIDs(platform_ids[p_id], CL_DEVICE_TYPE_ALL, 1, &device_id, &get_num_devices);

	// �������� ������ ��� ������ ���������� ���������
	cl_device_id* device_ids = (cl_device_id*)malloc(sizeof(cl_device_id)* get_num_devices);

	// �������� ������ ���� ��������� OpenCL ��������� ��� ���������
	ret = clGetDeviceIDs(platform_ids[p_id], CL_DEVICE_TYPE_ALL, get_num_devices, device_ids, &ret_num_devices);

	// ������� ������ ���������
	for (cl_uint i = 0; i < get_num_devices; ++i) {
		ret = clGetDeviceInfo(device_ids[i], CL_DEVICE_NAME, 80, device_name, NULL);
		printf("%d - %s\n", i, device_name);
	}

	int dev_id = 0;
	// ���������� ������� ����������, ���� �� ������ 1
	if (get_num_devices > 1) {

		do {
			printf("Please input device ID; default is 0.\nDevice ID: ");
			scanf("%d", &dev_id);
			printf("\n");
		} while (dev_id < 0 || dev_id >(ret_num_devices - 1));

	}

	// ������� ���������� � ��������� ����������
	ret = clGetDeviceInfo(device_ids[dev_id], CL_DEVICE_NAME, 80, device_name, NULL);
	cout << "Device Name " << device_name << endl;

	cl_uint maxComputeUnits;
	ret = clGetDeviceInfo(device_ids[dev_id], CL_DEVICE_MAX_COMPUTE_UNITS, sizeof(cl_uint), &maxComputeUnits, NULL);
	cout << "Compute Units " << maxComputeUnits << endl;

	cl_uint maxClockFrequency;
	ret = clGetDeviceInfo(device_ids[dev_id], CL_DEVICE_MAX_CLOCK_FREQUENCY, sizeof(cl_uint), &maxClockFrequency, NULL);
	cout << "Boost Clock " << maxClockFrequency << "MHz" << endl;

	return device_ids[dev_id];
}

int loadKernelFile(const char* fileName, size_t* source_size, char* source_str) {
	// ���������� � �������� kernel
	FILE *fp;

	// �������� ��������� ����, ������������ � kernel
	fp = fopen(fileName, "r");
	if (!fp)
	{
		fprintf(stderr, "Failed to load kernel.\n");
		return 1;
	}

	*source_size = fread(source_str, 1, MAX_SOURCE_SIZE, fp);
	fclose(fp);
	return 0;
}