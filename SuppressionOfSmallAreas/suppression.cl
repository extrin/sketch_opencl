bool HasBit(uint pattern, uint position)
{
	return pattern & (1 << position);
}

long FindMinLabel(__global uchar* bConn, __global uint* bLabels, uint Pos, uint width)
{
	uint x = Pos % width;
	uint y = Pos / width;
	uint height = get_global_size(0) / width;

	uint labels[8] = { 0, 0, 0, 0, 0, 0, 0, 0 };

	if (x > 0 && y > 0 && x < width - 1 && y < height - 1) {
		labels[0] = bLabels[x - 1 + (y - 1)*width];
		labels[1] = bLabels[x + (y - 1)*width];
		labels[2] = bLabels[x + 1 + (y - 1)*width];
		labels[3] = bLabels[x - 1 + y*width];
		labels[4] = bLabels[x + 1 + y*width];
		labels[5] = bLabels[x - 1 + (y + 1)*width];
		labels[6] = bLabels[x + (y + 1)*width];
		labels[7] = bLabels[x + 1 + (y + 1)*width];
	}

	uint L = bLabels[Pos];

	uchar bConnEl = bConn[Pos];

	uint minL = 2147483647;

	for (uint i = 0; i < 8; ++i)
	{
		if (HasBit(bConnEl, i) && labels[i] < minL) minL = labels[i];
	}

	return minL;
}


__kernel void blockMapInit(
	__global uchar* orig_img,
	__global uint* bLabels,
	__global uchar* bConn,
	__private uint width)
{
	uint coord = get_global_id(0);
	uint size = get_global_size(0);
	uint height = size / width;
	uint x = coord % width;
	uint y = coord / width;
	uint imgWidth = width * 2;
	uchar bConnItem = 0x0;
	uint bLabelsItem = 0;

	uint P = 0x0;
	uint P0 = 0x777;

	if (orig_img[2 * x + 2 * y*imgWidth] > 0) P = P | P0;
	if (orig_img[(2 * x + 1) + 2 * y*imgWidth] > 0) P = P | (P0 << 1);
	if (orig_img[2 * x + (2 * y + 1)*imgWidth] > 0) P = P | (P0 << 4);
	if (orig_img[(2 * x + 1) + (2 * y + 1)*imgWidth] > 0) P = P | (P0 << 5);

	if (P > 0) {
		bLabelsItem = coord + 1;
		if (x > 0 && y > 0 && x < (width - 1) && y < (height - 1)) {
			if (HasBit(P, 0x0) && orig_img[(2 * x - 1) + (2 * y - 1)*imgWidth] > 0)
				bConnItem |= 1 << 0x0;
			if ((HasBit(P, 0x1) && orig_img[2 * x + (2 * y - 1)*imgWidth] > 0) || (HasBit(P, 0x2) && orig_img[(2 * x + 1) + (2 * y - 1)*imgWidth] > 0))
				bConnItem |= 1 << 0x1;
			if (HasBit(P, 0x3) && orig_img[(2 * x + 2) + (2 * y - 1)*imgWidth] > 0)
				bConnItem |= 1 << 0x2;
			if ((HasBit(P, 0x4) && orig_img[(2 * x - 1) + 2 * y*imgWidth] > 0) || (HasBit(P, 0x8) && orig_img[(2 * x - 1) + (2 * y + 1)*imgWidth] > 0))
				bConnItem |= 1 << 0x3;
			if ((HasBit(P, 0x7) && orig_img[(2 * x + 2) + 2 * y*imgWidth] > 0) || (HasBit(P, 0xB) && orig_img[(2 * x + 2) + (2 * y + 1)*imgWidth] > 0))
				bConnItem |= 1 << 0x4;
			if (HasBit(P, 0xC) && orig_img[(2 * x - 1) + (2 * y + 2)*imgWidth] > 0)
				bConnItem |= 1 << 0x5;
			if ((HasBit(P, 0xD) && orig_img[2 * x + (2 * y + 2)*imgWidth] > 0) || (HasBit(P, 0xE) && orig_img[(2 * x + 1) + (2 * y + 2)*imgWidth] > 0))
				bConnItem |= 1 << 0x6;
			if (HasBit(P, 0xF) && orig_img[(2 * x + 2) + (2 * y + 2)*imgWidth] > 0)
				bConnItem |= 1 << 0x7;
		}
	}
	else bLabelsItem = 0;

	bConn[coord] = bConnItem;
	bLabels[coord] = bLabelsItem;
}

__kernel void scanningPhase(
	__global uint* bLabels,
	__global uchar* bConn,
	__private uint width,
	__global uint* isChanged)
{
	uint coord = get_global_id(0);
	uint L = bLabels[coord];

	if (L > 0)
	{
		uint Lmin = FindMinLabel(bConn, bLabels, coord, width);
		if (Lmin < L)
		{
			uint L1 = bLabels[L - 1];
			if (Lmin < L1) {
				bLabels[L - 1] = Lmin;
				atomic_inc(isChanged);
			}
		}
	}
}

__kernel void analysisPhase(
	__global uint* bLabels,
	__global uint* isChanged)
{
	uint coord = get_global_id(0);
	uint L = bLabels[coord];
	uint Lprev = L;

	if (L > 0)
	{
		uint Lcur = bLabels[L - 1];
		while (Lcur != L)
		{
			L = bLabels[Lcur - 1];
			Lcur = bLabels[L - 1];
		}
		bLabels[coord] = L;
		if (Lprev != L) {
			atomic_inc(isChanged);
		}
	}
}

__kernel void SetFinalLabels(
	__global uchar* orig_img,
	__global uint* bLabels,
	__global uint* labels,
	__private uint width)
{
	uint coord = get_global_id(0);
	uint x = coord % width;
	uint y = coord / width;
	const size_t spos = (x >> 1) + (y >> 1) * ceil((float)width / 2);

	labels[coord] = orig_img[coord] ? bLabels[spos] : 0;
}

__kernel void label_histogram(
	__global uint* labels,
	__global uint* histogram)
{
	int coord = get_global_id(0);
	atomic_inc(&histogram[labels[coord]]);
}

__kernel void suppression(
	__global uchar* res_image,
	__global uint* labels,
	__global uint* histogram,
	__private uint width)
{
	uint coord = get_global_id(0);

	if (labels[coord] && histogram[labels[coord]] > 20)
		res_image[coord] = 255;
	else
		res_image[coord] = 0;
}