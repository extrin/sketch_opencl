__kernel void erosia(
	__global unsigned char* temp_image,
	__private int W,
	__private int H,
	__global unsigned char* grey_out_image)
{
	int coord = get_global_id(0);
	int x = coord % W;
	int y = (coord - x) / W;

	if (x > 0 && x < (W - 1) && y > 0 && y < (H - 1)) {

		if (temp_image[coord] == 0) {
			grey_out_image[coord] = 0;
		}
		else {
			grey_out_image[coord] = 255;
			for (int l = -1; l < 2; ++l) {
				for (int t = -1; t < 2; ++t) {
					if (temp_image[(y + l) * W + (x + t)] == 0) {
						grey_out_image[coord] = 0;
						break;
					}
				}
			}
		}

	}
}