__kernel void dilatation(
	__global unsigned char* grey_image,
	__private int W,
	__private int H,
	__global unsigned char* temp_image)
{
	int coord = get_global_id(0);
	int x = coord % W;
	int y = (coord - x) / W;

	if (x > 0 && x < (W - 1) && y > 0 && y < (H - 1)) {

		if (grey_image[coord] == 255) {
			for (int l = -1; l < 2; ++l) {
				for (int t = -1; t < 2; ++t) {
					temp_image[(y + l) * W + (x + t)] = 255;
				}
			}
		}

	}
}