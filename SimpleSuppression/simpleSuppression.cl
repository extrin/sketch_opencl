__kernel void simpleSuppression(
	__global uchar* orig_img,
	__global uchar* res_img,
	__private uint width,
	__private uint height)
{
	int coord = get_global_id(0);
	int x = coord % width;
	int y = coord / width;

	if ((x + 9) < width && (y + 9) < height) {
		uchar pix = max(orig_img[coord], 
			max(orig_img[coord + 1], 
			max(orig_img[coord + 2], 
			max(orig_img[coord + 3], 
			max(orig_img[coord + 4], 
			max(orig_img[coord + 5], 
			max(orig_img[coord + 6],
			max(orig_img[coord + 7],
			max(orig_img[coord + 8],
			max(orig_img[x + (y + 1)*width], 
			max(orig_img[x + (y + 2)*width], 
			max(orig_img[x + (y + 3)*width], 
			max(orig_img[x + (y + 4)*width], 
			max(orig_img[x + (y + 5)*width], 
			max(orig_img[x + (y + 6)*width],
			max(orig_img[x + (y + 7)*width],
			max(orig_img[x + (y + 8)*width],
			max(orig_img[(x + 8) + (y + 1)*width],
			max(orig_img[(x + 8) + (y + 2)*width],
			max(orig_img[(x + 8) + (y + 3)*width],
			max(orig_img[(x + 8) + (y + 4)*width],
			max(orig_img[(x + 8) + (y + 5)*width], 
			max(orig_img[(x + 8) + (y + 6)*width],
			max(orig_img[(x + 8) + (y + 7)*width],
			max(orig_img[(x + 8) + (y + 8)*width],
			max(orig_img[(x + 1) + (y + 8)*width],
			max(orig_img[(x + 2) + (y + 8)*width],
			max(orig_img[(x + 3) + (y + 8)*width],
			max(orig_img[(x + 4) + (y + 8)*width], 
			max(orig_img[(x + 5) + (y + 8)*width],
			max(orig_img[(x + 6) + (y + 8)*width], orig_img[(x + 7) + (y + 8)*width])))))))))))))))))))))))))))))));
		if (pix == 0) {
			for (int i = x + 1; i < x + 8; ++i)
			{
				res_img[i + (y + 1)*width] = 0;
				res_img[i + (y + 2)*width] = 0;
				res_img[i + (y + 3)*width] = 0;
				res_img[i + (y + 4)*width] = 0;
				res_img[i + (y + 5)*width] = 0;
				res_img[i + (y + 6)*width] = 0;
				res_img[i + (y + 7)*width] = 0;
			}
		}
	}
}