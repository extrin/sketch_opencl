__kernel void RGBToGreyScale(
	__global uchar* r_image,
	__global uchar* g_image,
	__global uchar* b_image,
	__private int L,
	__global uchar* grey_out_image)
{
	int coord = get_global_id(0);
	float multR = 0.299,
		multG = 0.587,
		multB = 0.114;
	grey_out_image[coord] = r_image[coord] * multR + g_image[coord] * multG + b_image[coord] * multB;
}

__kernel void histogram(
	__global uchar* grey_image,
	__global uint* histogram)
{
	int coord = get_global_id(0);
	atomic_inc(&histogram[grey_image[coord]]);
}

__kernel void autoContrast(
	__global uchar* grey_orig_image,
	__global uchar* LUT,
	__global uchar* grey_res_image)
{
	int coord = get_global_id(0);
	grey_res_image[coord] = LUT[grey_orig_image[coord]];
}

__kernel void scanStep1(
	__global uchar* orig_img,
	__global uint* sum_img,
	__private int width)
{
	uint y = get_global_id(0);
	uint color = 0;
	uint tempVal = 0;
	for (int x = 0; x < width; x++)
	{
		color = orig_img[y * width + x];
		tempVal += color;
		sum_img[y * width + x] = tempVal;
	}
}

__kernel void scanStep2(
	__global uint* sum_img,
	__global uint* res_img,
	__private int height,
	__private int width)
{
	uint x = get_global_id(0);
	uint tempVal = 0;
	uint color = 0;
	for (int y = 0; y < height; y++)
	{
		color = sum_img[x + y * width];
		tempVal += color;
		res_img[x + y * width] = tempVal;
	}
}

__kernel void diffOfMeans(
	__global uint* integral_img,
	__global bool* res_img,
	__private int width,
	__private float num_mean_1,
	__private float num_mean_2,
	__private float step)
{
	int coord = get_global_id(0);
	int x = coord % width;
	int y = coord / width;
	int hs_1 = 1;
	int hs_2 = 2;

	float num1 = num_mean_1 * num_mean_1;
	float num2 = num_mean_2 * num_mean_2;

	uint sum = 0;

	if ((x - hs_1) > 0 && (y - hs_1) > 0) {
		sum = integral_img[(x + hs_1) + (y + hs_1) * width]
			+ integral_img[(x - hs_1 - 1) + (y - hs_1 - 1) * width]
			- integral_img[(x - hs_1 - 1) + (y + hs_1) * width]
			- integral_img[(x + hs_1) + (y - hs_1 - 1) * width];
	}
	else sum = integral_img[(x + hs_1) + (y + hs_1) * width];

	float first_mean = sum / num1;
	sum = 0;

	if ((x - hs_2) > 0 && (y - hs_2) > 0) {
		sum = integral_img[(x + hs_2) + (y + hs_2) * width]
			+ integral_img[(x - hs_2 - 1) + (y - hs_2 - 1) * width]
			- integral_img[(x - hs_2 - 1) + (y + hs_2) * width]
			- integral_img[(x + hs_2) + (y - hs_2 - 1) * width];
	}
	else sum = integral_img[(x + hs_2) + (y + hs_2) * width];

	float second_mean = sum / num2;

	float p = first_mean - second_mean;

	res_img[x + y * width] = p > step ? true : false;
}

__kernel void dilation(
	__global bool* grey_image,
	__private int W,
	__private int H,
	__global bool* temp_image)
{
	int coord = get_global_id(0);
	int x = coord % W;
	int y = (coord - x) / W;

	if (x > 0 && x < (W - 1) && y > 0 && y < (H - 1)) {

		if (grey_image[coord]) {
			temp_image[(y - 1) * W + (x - 1)] = true;
			temp_image[(y - 1) * W + x] = true;
			temp_image[(y - 1) * W + (x + 1)] = true;
			temp_image[y * W + (x - 1)] = true;
			temp_image[y * W + x] = true;
			temp_image[y * W + (x + 1)] = true;
			temp_image[(y + 1) * W + (x - 1)] = true;
			temp_image[(y + 1) * W + x] = true;
			temp_image[(y + 1) * W + (x + 1)] = true;
		}

	}
}

__kernel void erosia(
	__global bool* temp_image,
	__private int W,
	__private int H,
	__global bool* grey_out_image)
{
	int coord = get_global_id(0);
	int x = coord % W;
	int y = (coord - x) / W;
	bool pixel = temp_image[coord];

	if (x > 0 && x < (W - 1) && y > 0 && y < (H - 1)) {

		grey_out_image[coord] = pixel;

		bool temp;
		if (pixel) {
			temp = min(temp_image[(y - 1) * W + (x - 1)],
				min(temp_image[(y - 1) * W + x],
				min(temp_image[(y - 1) * W + (x + 1)],
				min(temp_image[y * W + (x - 1)],
				min(temp_image[y * W + x],
				min(temp_image[y * W + (x + 1)],
				min(temp_image[(y + 1) * W + (x - 1)],
				min(temp_image[(y + 1) * W + x], temp_image[(y + 1) * W + (x + 1)]))))))));
			grey_out_image[coord] = temp;
		}
	}
}

__kernel void simpleSuppression(
	__global bool* orig_img,
	__global bool* res_img,
	__private uint width,
	__private uint height)
{
	int coord = get_global_id(0);
	int x = coord % width;
	int y = coord / width;

	if ((x + 9) < width && (y + 9) < height) {
		bool pix = max(orig_img[coord],
			max(orig_img[coord + 1],
			max(orig_img[coord + 2],
			max(orig_img[coord + 3],
			max(orig_img[coord + 4],
			max(orig_img[coord + 5],
			max(orig_img[coord + 6],
			max(orig_img[coord + 7],
			max(orig_img[coord + 8],
			max(orig_img[x + (y + 1)*width],
			max(orig_img[x + (y + 2)*width],
			max(orig_img[x + (y + 3)*width],
			max(orig_img[x + (y + 4)*width],
			max(orig_img[x + (y + 5)*width],
			max(orig_img[x + (y + 6)*width],
			max(orig_img[x + (y + 7)*width],
			max(orig_img[x + (y + 8)*width],
			max(orig_img[(x + 8) + (y + 1)*width],
			max(orig_img[(x + 8) + (y + 2)*width],
			max(orig_img[(x + 8) + (y + 3)*width],
			max(orig_img[(x + 8) + (y + 4)*width],
			max(orig_img[(x + 8) + (y + 5)*width],
			max(orig_img[(x + 8) + (y + 6)*width],
			max(orig_img[(x + 8) + (y + 7)*width],
			max(orig_img[(x + 8) + (y + 8)*width],
			max(orig_img[(x + 1) + (y + 8)*width],
			max(orig_img[(x + 2) + (y + 8)*width],
			max(orig_img[(x + 3) + (y + 8)*width],
			max(orig_img[(x + 4) + (y + 8)*width],
			max(orig_img[(x + 5) + (y + 8)*width],
			max(orig_img[(x + 6) + (y + 8)*width], orig_img[(x + 7) + (y + 8)*width])))))))))))))))))))))))))))))));
		if (!pix) {
			for (int i = x + 1; i < x + 8; ++i)
			{
				res_img[i + (y + 1)*width] = false;
				res_img[i + (y + 2)*width] = false;
				res_img[i + (y + 3)*width] = false;
				res_img[i + (y + 4)*width] = false;
				res_img[i + (y + 5)*width] = false;
				res_img[i + (y + 6)*width] = false;
				res_img[i + (y + 7)*width] = false;
			}
		}
	}
}

__kernel void blending(
	__global bool* mask,
	__global uchar* orig_r,
	__global uchar* orig_g,
	__global uchar* orig_b,
	__global uchar* res_r,
	__global uchar* res_g,
	__global uchar* res_b)
{
	uint coord = get_global_id(0);

	res_r[coord] = 0;
	res_g[coord] = 0;
	res_b[coord] = 0;
	if (mask[coord]) {
		res_r[coord] = orig_r[coord];
		res_g[coord] = orig_g[coord];
		res_b[coord] = orig_b[coord];
	}
}
