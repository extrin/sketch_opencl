__kernel void gaussianBlur(
					__global unsigned char* r_image, 
				    __global unsigned char* g_image, 
				    __global unsigned char* b_image,
				    __private int W,
				    __private int H,
				    __global float* mask,
				    __private int maskSize,
				    __global unsigned char* r_out_image,
				    __global unsigned char* g_out_image,
				    __global unsigned char* b_out_image)
{
	int L = H * W;
	int coord = get_global_id(0);
	int x = coord % W;
	int y = (coord - x) / W;

	int maskWidth = (maskSize * 2 + 1);

	if (coord < L) {

		//���������� �����
		float S_R = 0.0, 
			   S_G = 0.0, 
			   S_B = 0.0;

		for(int a = -maskSize; a < maskSize + 1; ++a) {
			for(int b = -maskSize; b < maskSize + 1; ++b) {
				float M = mask[a + maskSize + (b + maskSize) * maskWidth];
				if (y >= maskSize && x >= maskSize && y < (H - maskSize) && x < (W - maskSize)) {
					S_R += r_image[(y + b) * W + (x + a)] * M;
					S_G += g_image[(y + b) * W + (x + a)] * M;
					S_B += b_image[(y + b) * W + (x + a)] * M;
				}
			}
		}

		//������� ���������� ������ � �������� �������
		if (y >= maskSize && x >= maskSize && y < (H - maskSize) && x < (W - maskSize)) {
			r_out_image[x + y * W] = S_R;
			g_out_image[x + y * W] = S_G;
			b_out_image[x + y * W] = S_B;
		}
		else {
			r_out_image[x + y * W] = r_image[x + y * W];
			g_out_image[x + y * W] = g_image[x + y * W];
			b_out_image[x + y * W] = b_image[x + y * W];
		}

	}
}
